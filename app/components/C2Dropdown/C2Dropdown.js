import {Component, View, NgFor, NgIf, ElementRef,EventEmitter} from 'angular2/angular2';
import {C2DropdownMenu} from '../C2Dropdownmenu/C2DropdownMenu';
import {C2DropdownItem} from './C2DropdownItem/C2DropdownItem';

@Component({
  selector    : 'c2-dropdown',
  properties  : ['items','selected'],
  host: {
    '(^mousedown)' : 'toggleHide($event)',
    '[class]':'theClass'
   },
   events:['selectedEv : selected']
   
   
})
@View({
  templateUrl: './components/C2Dropdown/C2Dropdown.html',
  directives:[NgFor,C2DropdownMenu,C2DropdownItem]
})
export class C2Dropdown {
  
  constructor(  elRef:ElementRef) {
    this.menu = null;
    
    var el:HTMLElement = elRef.nativeElement;
    
    this.el = el;
    this.selectedEv = new EventEmitter();
    
    console.log(' in c2 dd',el);
     
  }
  
  addMenu(menu:C2DropdownMenu) {
    console.log('menu added',menu,this.items);
    this.menu = menu;
  }
  
  toggleHide($event) {
    // if left clicked
    if (event.which === 1) {
      this.menu.toggleHide($event);
      // add an open class so that we have the right bootstrap sequence to open the menu
      if (!this.menu.mehidden) {
       this.el.children[0].className = 'dropdown open';
      }
      else {
        this.el.children[0].className = 'dropdown';
        this.selectedEv.next($event.target.textContent)
      }
      console.log($event.target);
      
    }
  }
  
  onSelected($event) {
    console.log('selected',$event);
    
  }
}

