import {Host,Directive} from 'angular2/angular2';
import {C2Dropdown} from '../C2Dropdown';


@Directive ({
  selector: "[dropdownitem]",
  host: {
    '(^mousedown)' :'onSelected($event)'
  }
})

export class C2DropdownItem {
  constructor(ancestor:C2Dropdown){
     console.log(' in item dd',ancestor);
     // this is going to be the way to communicate
     this.ancestor = ancestor;
  }
  
  onSelected($event) {
    console.log('selected',$event);
    this.ancestor.onSelected($event);
  }
}
