import {Host,Directive,Component,View} from 'angular2/angular2';
import {C2Dropdown} from '../C2Dropdown/C2Dropdown';


@Directive ({
  selector: "[dropdownmenu]",
  // host: {
  //   '[hidden]':'hidden'
  // }
})



export class C2DropdownMenu {
 
  constructor(@Host() ancestor:C2Dropdown){
     console.log(' in c2 ddmenu',ancestor);
     // this is going to be the way to communicate
     ancestor.addMenu(this);
     this.mehidden = true;
  }
  
  toggleHide($event) {
    console.log('toggleHide',$event);
    this.mehidden = !this.mehidden;
    
  }
  
} 
